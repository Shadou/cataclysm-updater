# Simple Cataclysm: DDA updater for GNU/Linux

## Usage
* `./updater update` to update, also copies the previous version to a backup folder.
* `./updater start` to launch the game.

## Config
Configuration resides in the `config` file.

The `config` file may exist either in `$XDG_CONFIG_HOME/cataclysm-updater/config` or in the root directory of the updater script.
* `CATA_DIRECTORY`, install location.
* `KEEP_CONFIG`, keep old game configs when updating.
